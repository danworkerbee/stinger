 
    <div class="container-fluid" id="spi-body-container"> 
        <div class="row" id="spi-row-content">
             <div class="col-12 col-lg-4" style="text-align: center;float: none;margin:0 auto;">
                 
                 <div id="sti-header-flex-div">
                      <div class="sti-header-flex">
                       <img src="/images/Stinger_Productions_Logo_INC_WHITE.png" title="Stinger Productions Inc. Logo" class="spi-logo-img img-fluid" />
                         <h2>New Website...</h2>
                         <h3>Coming Soon</h3>
                    </div>
                  </div>
                 
                 
              </div>
        </div>
     </div>
     <div class="container-fluid" id="spi-footer-container"> 
        <div class="row" id="">
          <div class="col-12 col-lg-12">
             <p style="text-align: right;color: #fff;">&copy; Stinger Productions Inc. <?php echo date("Y"); ?></p>
          </div>
        </div>
     </div> 