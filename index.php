<?php
/**
 * filename: header.php
 * description: this will have all the information needed on the header of each page
 * author: Jullie Quijano
 * date created: 2014-03-24
 */
 
 
$wp_test = $_GET['wb_test'];
 
 ?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/style.css" integrity="" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,900|Roboto+Slab" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700&display=swap" rel="stylesheet">

    <title>Stinger Productions Inc.</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-89223814-2"></script>


<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-89223814-2');
</script>

  </head>
  <body>
    <h1></h1>
       <div class="container" id="spi-head-container"> 
        <div class="row" id="">
          <div class="col-12 col-lg-12">
             <img src="/images/Stinger_Productions_Logo_FINAL-01.png" title="Stinger Productions Inc. Logo" class="img-fluid"  />
                <?php
     
        if(isset($wp_test) && trim($wp_test == 'wbtv')) {
            
        ?>
             <div id="spi-top-menu-right" style="color: #000;">
              <ul>
                <li><a href="#spi-rs-container">Services <i class="fas fa-chevron-down"></i></a></li>
                <li><a href="#spi-contact-container">Contact</a></li>
              </ul>
             </div>
             
             <?php
             }
             ?>
          </div>
        </div>
     </div>  
     <?php
     
        if(isset($wp_test) && trim($wp_test == 'wbtv')) {
            include 'home.php';
        }else{
           include 'coming-soon.php';
        }
     
     ?>
     
       
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
  </body>
</html>