 
<div class="container-fluid spi-style-container" id="spi-wwd-container"> 
  <div class="row spi-row-center" id="spi-wwd-row-content">
       <div class="col-12 col-lg-12">
         <div id="spi-wwd-flex-div">
                  <div class="spi-wwd-flex">
        
                    <img src="/images/SPI-Logo-White.png" title="Stinger Productions Inc. Logo" class="spi-logo-img img-fluid" />
        
                    
            
                     
                     <h2> What We Do</h2>
                     <p>Stinger Production Inc. (Stinger Productions) is in the business of supporting organizations
                      producing documentaries for both traditional and digital distribution. Documentary makers 
                      can rely on Stinger Productions for a wide variety of services to complement their needs.                     
                    <p>     
          
                 
                </div>
              </div>
        </div>     
  </div>
</div>   <!-- end of spi-wwd-container -->

 
<div class="container-fluid spi-style-container" id="spi-rs-container"> 
  <div class="row " id="spi-rs-row-content">
       <div id="spi-rs-col-left" class="col-12 col-lg-6" >           
           <div id="spi-rs-left-flex-div">
                <div class="spi-rs-flex">                 
                   <img src="/images/Rental-Services-Icon.png" title="Rental Services Icon" class="spi-logo-img img-fluid" />
                   <h2>Rental Services</h2>                   
              </div>
            </div>           
        </div>
        
        <div id="spi-rs-col-right" class="col-12 col-lg-6">           
           <div id="spi-rs-right-flex-div">
                <div class="spi-rs-flex">
                 
                   <ul>
                      <li>Camera Equipment</li>
                      <li>Edit Suites</li>
                      <li>Facilities</li>
                      <li>Studio (with green screen)</li>
                      <li>Hosting and Storage</li>
                   </ul>
                   
              </div>
            </div>           
        </div>        
  </div>
</div>

<div class="container-fluid spi-style-container" id="spi-pcs-container"> 
  <div class="row" id="spi-pcs-row-content">
       <div class="col-12 col-lg-12" >           
           <div  id="spi-pcs-flex-div" class="content-2-icons text-center">                      
                  <img src="/images/Creative-Services-Icon.png" title="Project & Creative Services Icon" class="spi-logo-img img-fluid" />
                   <h2 class="h2-gray">Project & Creative Services</h2>                   
            </div>
           <!--
           <div id="spi-pcs-flex-div">
                <div class="spi-pcs-flex text-center">
                   <img src="/images/Creative-Services-Icon.png" title="Project & Creative Services Icon" class="spi-logo-img img-fluid" />
                   <h2 class="h2-gray">Project & Creative Services</h2>
                   
              </div>
            </div>     
            -->      
        </div>
        
        <div class="col-12 col-lg-6 spi-pcs-col-contents" id="spi-pcs-filming">           
           <div id="spi-pcs-f-flex-div" class="spi-pcs-flex-div-main"> 
                <div class="sti-pcs-box-flex">
                  <h3>Filming</h3>
                  <div style="border-bottom: 3px solid #ffe12f;width:50%;"></div>
                   <ul>
                      <li>Traditional filming services</li>
                      <li>Remote filming services</li>                      
                   </ul>                  
              </div>
            </div>           
        </div>
        
        <div class="col-12 col-lg-6 spi-pcs-col-contents" id="spi-pcs-content-dev">           
           <div id="spi-pcs-cd-flex-div" class="spi-pcs-flex-div-main">
                <div class="sti-pcs-box-flex">
                 
                     <h3>Content Development</h3>
                     <div style="border-bottom: 3px solid #ffe12f;width:50%;"></div>
                   <ul>
                      <li>Scripting and Storyboarding</li>
                      <li>Creative Story Development</li>                      
                   </ul> 
                   
              </div>
            </div>           
        </div>
        
        <div class="col-12 col-lg-6 spi-pcs-col-contents" id="spi-pcs-video-services">           
           <div id="spi-pcs-vs-flex-div" class="spi-pcs-flex-div-main">
                <div class="sti-pcs-box-flex">
                 
                     <h3>Video Services</h3>
                     <div style="border-bottom: 3px solid #ffe12f;width:50%;"></div>
                   <ul>
                      <li>Graphics Packages</li>
                      <li>2D and 3D Animation</li>
                      <li>Music Packages</li>
                      <li>Documentary Editing</li>                      
                   </ul> 
                   
              </div>
            </div>           
        </div>
        
        <div class="col-12 col-lg-6 spi-pcs-col-contents" id="spi-pcs-project-management">           
           <div id="spi-pcs-pm-flex-div" class="spi-pcs-flex-div-main">
                <div class="sti-pcs-box-flex">
                 
                     <h3>Project Management</h3>
                     <div style="border-bottom: 3px solid #ffe12f;width:50%;"></div>
                   <ul>
                      <li>Directing</li>
                      <li>Producing</li>
                      <li>Administrative Services</li>                      
                   </ul> 
                   
              </div>
            </div>           
        </div>
        
  </div>
</div>


 
<div class="container-fluid spi-style-container" id="spi-contact-container"> 
  <div class="row spi-row-center" id="spi-contact-row-content">
       <div class="col-12 col-lg-6">
         <div id="spi-contact-flex-div">
                  <div class="spi-contact-flex">
        
          
                 
                </div>
              </div>
        </div>
        
        
        <div class="col-12 col-lg-6">
         <div id="spi-contact-img-flex-div">
                  <div class="spi-contact-img-flex">
        
                    <img src="/images/SP-Google-Map.png" title="Stinger Productions Inc. Logo" class="spi-logo-img img-fluid" />
        
                    
            
                     
                   
                 
                </div>
              </div>
        </div>     
  </div>
</div>   <!-- end of spi-wwd-container -->


<div class="container-fluid spi-style-container" id="spi-footer-container"> 
  <div class="row spi-row-center" id="spi-footer-row-content">
       <div class="col-12 col-lg-6">
        <ul>
          <li><a class="a-services" href="#spi-rs-container">Services</a></li>
          <li><a href="#spi-contact-container">Contact</a></li>
        </ul>
        </div>
        
        
        <div class="col-12 col-lg-6 text-copyright">
              &copy; Stinger Productions Inc. <?php echo date('Y'); ?>
              </div>
        </div>     
  </div>
</div>   <!-- end of spi-wwd-container -->